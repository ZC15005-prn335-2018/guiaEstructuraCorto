#include <stdio.h>

int main(void)
{
    int tam;
    printf("de que  tamaño sera la matriz\n");
    scanf("%d", &tam);
    int matriz1[tam][tam],matriz2[tam][tam],matriz3[tam][tam];
    for (int j = 0; j < tam; j++)
    {
        for (int c = 0; c< tam; c++)
        {
            printf("Ingrese el valor en la matriz posicion # %d, %d\n", (j + 1), (c + 1));
            scanf("%d", &matriz1[j][c]);
        }
    }
    for (int j = 0; j < tam; j++)
    {
        for (int c = 0; c < tam; c++)
        {
            printf("Ingrese el valor en la matriz posicion # %d, %d\n", (j+ 1), (c + 1));
            scanf("%d", &matriz2[j][c]);
        }
    }
    //tercera matriz con el resultado de las multiplicaciones
    printf("\n matriz resultante:\n");
    for (int j= 0; j < tam; j++)
    {
        for (int c = 0; c < tam; c++)
        {
            matriz3[j][c] = matriz1[j][c]*matriz2[j][c];
            printf("%d\t",matriz3[j][c]);
        }
        printf("\n");
    }
    //numeros primos generan un vector con el tamaño
    int tamv = 0;
    for (int j = 0; j < tam; j++)
    {
        for (int c = 0; c < tam; c++)
        {
            // es primo
            int contador=0;
            for(int f=1; f <=matriz3[j][c]; f++)
            {
                //un primo solo se divide entre 1 y el mismo
                if(matriz3[j][c]%f==0)
                {
                    contador++;
                }
            }
            if(contador ==2)
            {
                tamv ++;
            }
        }
    }

    //llena el vector con los  numeros encontrados primos
    if(tamv >=1)
    {
        int vector[tamv],itv = 0;
        for (int j = 0; j < tam; j++)
        {
            for (int c = 0; c < tam; c++)
            {
                //es primo, otra vez
                int contador=0;
                for(int f=1; f<=matriz3[j][c]; f++)
                {
                    //un  numero primo solo se  dividie entre 1 y el mismo
                    if(matriz3[j][c]%f==0)
                    {
                        contador ++;
                    }
                }
                if(contador ==2)
                {
                    vector[itv] = matriz3[j][c];
                    printf(" numero Primo en la posicion # %d, %d y es: %d\n",(j+1),(c+1), vector[itv]);
                    itv++;
                }
            }
        }
        //ordenamos  el vector
        printf("vector deforma ascendente \n");
        if(tamv>1)
        {
            int bubble;
            for (int m=0; m<(tamv-1); m++)
            {
                if(vector[m]>vector[m+1])
                {
                    bubble = vector[m];
                    vector[m] = vector [m+1];
                    vector[m+1] = bubble;
                }
            }
        }
        printf("vector ordenado :\n");
        for (int m=0; m<tamv; m++)
        {
            printf("%d \t",vector[m]);
        }
    }
    else
    {
        printf("No hay numeros primos");
    }
}



